$(function(){
	function setFooter()
	{
		var doc_height = $(document).height();
		var win_height = $(window).height();
		var header_height = $("#header").height();
		var page_height = $("#page").height();
		var footer_height = $(".footer").height();
		var all_height = page_height + footer_height + header_height;
		
		setTimeout(function(){
			if(doc_height <= win_height)
			{	
				$(".footer").css("margin-top", ((doc_height - all_height) - 42) + "px");
			}
		}, 100);
	}
	setFooter();
	
	$( window ).resize(function() {
		setFooter();
	});	
});