$(function(){
	$(".nominal").inputmask('currency',{prefix:'',digits:0});
	var data_cus = [];
	$.ajax({
		url:'http://www.w3schools.com/angular/customers.php',
		type:'GET',
		dataType:'JSON',
		async: true,
		success: function(data){
			$.each(data.records, function(index, value ) {
				_yw1_source_list.push(value.Name);
				data_cus[value.Name] = {
					'city':value.City,
					'country':value.Country
				};
			});
		}
	});
	$('#yw1').bind('typeahead:select', function(ev, suggestion){
		console.log('Selection: ' + suggestion);
	});
	
	$('#yw1').on('change', function(event){
		if(data_cus[$(this).val()] != undefined)
		{
			$.each(data_cus[$(this).val()], function(index, value){
				$('form').find("input[name*='["+ index +"]']").val(value);
			});
		}
	});	
});