<?php

/**
 * This is the model class for table "email_log".
 *
 * The followings are the available columns in table 'email_log':
 * @property integer $id
 * @property string $dest
 * @property string $t
 * @property string $status
 * @property string $errormsg
 */
class EmailLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dest', 'length', 'max'=>64),
			array('status', 'length', 'max'=>45),
			array('t, errormsg', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dest, t, status, errormsg', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dest' => 'Dest',
			't' => 'T',
			'status' => 'Status',
			'errormsg' => 'Errormsg',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dest',$this->dest,true);
		$criteria->compare('t',$this->t,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('errormsg',$this->errormsg,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}