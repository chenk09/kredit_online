<?php

/**
 * This is the model class for table "trx_pengajuan".
 *
 * The followings are the available columns in table 'trx_pengajuan':
 * @property integer $id_pengajuan
 * @property integer $id_customer
 * @property string $status_pengajuan
 * @property integer $penghasilan
 */
class TrxPengajuan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trx_pengajuan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_customer, penghasilan', 'required'),
			array('id_customer', 'numerical', 'integerOnly'=>true),
			array('status_pengajuan', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tanggal_pengajuan, id_pengajuan, id_customer, status_pengajuan, penghasilan', 'safe', 'on'=>'search'),
			array(
				'status_pengajuan',
				'default',
				'value'=>'PROSES',
				'setOnEmpty'=>false,'on'=>'insert'
			),
			array(
				'tanggal_pengajuan',
				'default',
				'value'=>new CDbExpression("datetime()"),
				'setOnEmpty'=>false,'on'=>'insert'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'IdCustomer' => array(self::BELONGS_TO, 'MstCustomers', 'id_customer'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pengajuan' => 'ID',
			'id_customer' => 'ID Customer',
			'status_pengajuan' => 'Status Pengajuan',
			'penghasilan' => 'Penghasilan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pengajuan',$this->id_pengajuan);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('status_pengajuan',$this->status_pengajuan,true);
		$criteria->compare('penghasilan',$this->penghasilan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrxPengajuan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			foreach($this->metadata->tableSchema->columns as $columnName => $column)
			{
				if(!is_object($this->$columnName))
				{
					if (strtolower($column->dbType) == 'date')
					{
						$this->$columnName = date('Y-m-d', strtotime($this->$columnName));
						$this->$columnName = new CDbExpression("to_date('" . $this->$columnName . "','YYYY-MM-DD')");
					}
					elseif (strtolower($column->dbType) == 'datetime')
					{
						$this->$columnName = date('Y-m-d H:i:s',
							CDateTimeParser::parse($this->$columnName, "dd/mm/yyyy  H:i:s"));
					}
					elseif (strtolower($column->dbType) == 'number')
					{
						$this->$columnName = str_replace(",", "", $this->$columnName);
					}
				}
			}
			return true;
		}else return false;
	}
	
	protected function afterFind()
	{
		foreach($this->metadata->tableSchema->columns as $columnName => $column)
		{
			if(!is_object($this->$columnName))
			{
				if (strtolower($column->dbType) == 'date')
				{
					$this->$columnName = date('m/d/Y', strtotime($this->$columnName));
				}
				elseif (strtolower($column->dbType) == 'datetime')
				{
					$this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, "dd/mm/yyyy  H:i:s"));
				}
				elseif (strtolower($column->dbType) == 'number')
				{
					$this->$columnName = number_format($this->$columnName, 0, ".", ",");
				}
			}
		}
		return parent::afterFind();
	}	
	
}
