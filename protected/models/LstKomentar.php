<?php

/**
 * This is the model class for table "lst_komentar".
 *
 * The followings are the available columns in table 'lst_komentar':
 * @property integer $id_komentar
 * @property integer $id_pengajuan
 * @property string $komentar
 * @property string $status_pengajuan
 * @property string $user_komentar
 * @property string $tanggal_komentar
 */
class LstKomentar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lst_komentar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('komentar, status_pengajuan', 'required'),
			array('id_pengajuan', 'numerical', 'integerOnly'=>true),
			array('komentar, status_pengajuan, user_komentar', 'length', 'max'=>32),
			array('tanggal_komentar', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_komentar, id_pengajuan, komentar, status_pengajuan, user_komentar, tanggal_komentar', 'safe', 'on'=>'search'),
			array(
				'tanggal_komentar',
				'default',
				'value'=>new CDbExpression("datetime()"),
				'setOnEmpty'=>false,'on'=>'insert'
			),
			array(
				'user_komentar',
				'default',
				'value'=>Yii::app()->user->id,
				'setOnEmpty'=>false,'on'=>'insert'
			)			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_komentar' => 'Id Komentar',
			'id_pengajuan' => 'Id Pengajuan',
			'komentar' => 'Komentar',
			'status_pengajuan' => 'Status Pengajuan',
			'user_komentar' => 'User Komentar',
			'tanggal_komentar' => 'Tanggal Komentar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_komentar',$this->id_komentar);
		$criteria->compare('id_pengajuan',$this->id_pengajuan);
		$criteria->compare('komentar',$this->komentar,true);
		$criteria->compare('status_pengajuan',$this->status_pengajuan,true);
		$criteria->compare('user_komentar',$this->user_komentar,true);
		$criteria->compare('tanggal_komentar',$this->tanggal_komentar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LstKomentar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function afterSave()
	{
		if($this->user_komentar == "manager")
		{
			$trxPengajuan = TrxPengajuan::model()->findByPk($this->id_pengajuan);
			if($trxPengajuan)
			{
				$email = new Email();
				$message = "PENGAJUAN SAUDARA SAYA " . $this->status_pengajuan . " : " . $this->komentar;
				$email->send($trxPengajuan->IdCustomer->email, $message);
			}
		}
		parent::afterSave();
	}
	
}
