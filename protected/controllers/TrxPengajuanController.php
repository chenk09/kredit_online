<?php

class TrxPengajuanController extends Controller
{
public function filters()
{
	return array(
	'accessControl', // perform access control for CRUD operations
	);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
	return array(
		array(
			'allow',
			'actions'=>array('admin','delete','index','view', 'create','update'),
			'users'=>array('*'),
		),
		array('deny',  // deny all users
			'users'=>array('*'),
		),
	);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
	$mstCustomers = new MstCustomers;
	$model = new TrxPengajuan;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

	if(isset($_POST['MstCustomers']))
	{
		$mstCustomers->attributes = $_POST['MstCustomers'];
		$mstCustomers->save();
		
		//$model->attributes = $_POST['TrxPengajuan'];
		//if($model->save()) $this->redirect(array('view','id'=>$model->id_pengajuan));
		
	}

	$this->render('create',array(
		'model'=>$model,
		'mstCustomers'=>$mstCustomers,
	));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['TrxPengajuan']))
{
$model->attributes=$_POST['TrxPengajuan'];
if($model->save())
$this->redirect(array('view','id'=>$model->id_pengajuan));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
	$dataProvider=new CActiveDataProvider('TrxPengajuan');
	$this->render('index',array(
		'dataProvider'=>$dataProvider,
	));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new TrxPengajuan('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['TrxPengajuan']))
$model->attributes=$_GET['TrxPengajuan'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=TrxPengajuan::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='trx-pengajuan-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
