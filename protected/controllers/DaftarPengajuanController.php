<?php

class DaftarPengajuanController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions'=>array('index','create'),
				'users'=>array('@'),
			),
			array(
				'deny',
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$model=new TrxPengajuan('search');
		$model->unsetAttributes();
		
		if(Yii::app()->user->id_sys_group == 'petugas_satu')
		{
			$model->status_pengajuan = 'PROSES';
		}elseif(Yii::app()->user->id_sys_group == 'petugas_dua')
		{
			$model->status_pengajuan = 'PETUGAS DUA';
		}else{
			$model->status_pengajuan = 'MANAGER';
		}
		
		if(isset($_GET['TrxPengajuan']))
			$model->attributes=$_GET['TrxPengajuan'];
		
		$this->render('index',array(
			'model'=>$model,
		));
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='trx-pengajuan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionCreate($id = null)
	{
		if(is_null($id))
		{
			Yii::app()->user->setFlash('error', "<strong>Pengajuan kosong</strong> Coba cek ulang kode pengajuan.");
			$this->redirect(array('index'));
		}
		
		$lstKomentar = new LstKomentar;
		$model = TrxPengajuan::model()->findByPk($id);
		$mstCustomers = MstCustomers::model()->findByPk($model->id_customer);

		if(isset($_POST["LstKomentar"]))
		{
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			
			try
			{
				$lstKomentar->attributes = $_POST["LstKomentar"];
				if(Yii::app()->user->id_sys_group == 'petugas_satu')
				{
					$status_pengajuan = "PETUGAS DUA";
				}elseif(Yii::app()->user->id_sys_group == 'petugas_dua')
				{
					$status_pengajuan = "MANAGER";
				}else{
					$status_pengajuan = "SELESAI";
				}
				
				if(isset($_POST["yt1"]))
				{
					$status_pengajuan = "TOLAK";
				}
				$lstKomentar->status_pengajuan = $status_pengajuan;
				$lstKomentar->id_pengajuan = $model->id_pengajuan;
				$model->status_pengajuan = $status_pengajuan;
				
				if(!$lstKomentar->save())
				{
					foreach($lstKomentar->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				
				if(!$model->save())
				{
					foreach($model->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				
				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}else{
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Proses pengajuan berhasil");
					$this->redirect(array('index'));
				}				
				
			}catch(Exception $e){
				$transaction->rollback();
				Yii::app()->user->setFlash('danger', "Proses gagal = <br>{$e->getMessage()}");
			}
			
		}
		
		$this->render('create',array(
			"lstKomentar"=>$lstKomentar,
			"mstCustomers"=>$mstCustomers,
			"trxPengajuan"=>$model
		));
	}
	
}
