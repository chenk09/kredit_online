<?php

class PengajuanController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}	
	
	public function actionIndex()
	{
		$_js_file =  "scripts/input-mask/inputmask.js";
		Yii::app()->getClientScript()->registerScriptFile($_js_file, CClientScript::POS_END);
		
		$_js_file =  "scripts/input-mask/inputmask.numeric.extensions.js";
		Yii::app()->getClientScript()->registerScriptFile($_js_file, CClientScript::POS_END);
		
		$_js_file =  "scripts/input-mask/jquery.inputmask.js";
		Yii::app()->getClientScript()->registerScriptFile($_js_file, CClientScript::POS_END);
		
		$_js_file =  "scripts/typeahead/js/typeahead.bundle.min.js";
		Yii::app()->getClientScript()->registerScriptFile($_js_file, CClientScript::POS_END);
		
		
		$model = new MstCustomers;
		$trxPengajuan = new TrxPengajuan;
		if(isset($_POST["MstCustomers"]))
		{
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			
			try
			{
				$model->attributes = $_POST["MstCustomers"];
				if(!$model->save())
				{
					foreach($model->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				
				$trxPengajuan->attributes = $_POST["TrxPengajuan"];
				$trxPengajuan->id_customer = $model->id_customer;
				if(!$trxPengajuan->save())
				{
					foreach($trxPengajuan->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				
				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}else{
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Proses pengajuan berhasil");
					$this->redirect(array('index'));
				}
			}catch(Exception $e){
				$transaction->rollback();
				Yii::app()->user->setFlash('danger', "Proses gagal = <br>{$e->getMessage()}");
			}
			
		}
		
		$this->render('index',array("model"=>$model, "trxPengajuan"=>$trxPengajuan));
	}
	
}