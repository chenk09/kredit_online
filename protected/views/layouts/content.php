<?php $this->beginContent('//layouts/main'); ?>
<div id="header">
	<?php
		$this->widget(
			'booster.widgets.TbNavbar',
			array(
				'brand' => '<i class="glyphicon glyphicon-file"></i> ' . Yii::app()->name,
				'fixed'=>false,
				'items' => array(
					array(
						'class' => 'booster.widgets.TbMenu',
						'type' => 'navbar',
						'items' => array(
							array('icon'=>'file','label'=>'Pengajuan Kredit', 'url'=>array('/pengajuan')),
							array('icon'=>'list','label'=>'Daftar Pengajuan', 'url'=>array('/daftarPengajuan')),
						)
					),
					array(
						'class' => 'booster.widgets.TbMenu',
						'type' => 'navbar',
						'encodeLabel' => false,
						'htmlOptions' => array('class' => 'pull-right'),
						'items' => array(
							array(
								'label'=>'Logout ('.Yii::app()->user->name.')', 
								'url'=>array('/site/logout'),
								'visible'=>!Yii::app()->user->isGuest,
							),
						)
					)
				)
			)
		);
	?>
</div>
<div style="margin-top:25px" class="container" id="page">
	<?php $this->widget('booster.widgets.TbAlert', array(
		'fade' => true,
		'closeText' => '&times;',
		'events' => array(),
		'htmlOptions' => array(),
	));?>
	<?php if(isset($this->breadcrumbs)):?>
	<?php $this->widget(
		'booster.widgets.TbBreadcrumbs',
		array(
			'links'=>$this->breadcrumbs
		)
	);?>
	<?php endif?>
	
	<?php if(isset($this->menu) && count($this->menu) > 20):?>
		<div class="well">
			<div class="row">
				<div class="pull-left">
					<?php $this->widget(
						'booster.widgets.TbButtonGroup',
						array(
							'buttonType' => 'link',
							'buttons' =>$this->menu,
						)
					);?>
				</div>
				<div class="pull-right">
					<?php $this->widget(
						'booster.widgets.TbButtonGroup',
						array(
							'context' => 'success',
							'buttons' => array(
								array('label' => 'Export Informasi', 'url' => '#'),
								array(
									'items' => array(
										array('label' => 'PDF', 'url' => '#'),
										array('label' => 'Excel', 'url' => '#'),
									)
								),
							),
						)
					);?>
				</div>
			</div>
		</div>
<?php endif?>
	<?php echo $content; ?>
</div><!-- page -->

<footer class="footer">
	<div class="container">
		<div>Copyright &copy; <?php echo date('Y'); ?> by Dana Pensiun Telkom. All Rights Reserved.</div>
		<div class="copy"><?php echo Yii::powered(); ?></div>
	</div>
</footer><!-- footer -->

	<?php $this->beginWidget(
		'booster.widgets.TbModal',
		array('id'=>'myModal')
	); ?>
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h4>-</h4>
		</div>
		<div class="modal-body"><h4>Loading.....</h4></div>
	<?php $this->endWidget(); ?>

<?php $this->endContent(); ?>