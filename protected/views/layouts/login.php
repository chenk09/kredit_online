<?php $this->beginContent('//layouts/main'); ?>
<?php $this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;',
	'events' => array(),
	'htmlOptions' => array(),
));?>
<div class="container"><?php echo $content; ?></div>
<?php $this->endContent(); ?>