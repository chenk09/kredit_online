<?php
$this->pageTitle = Yii::app()->name;
?>
<div class="well">
	<h4>Selamat datang di aplikasi <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h4>
	<hr>
	<div class="row">
		<div class="col-sm-4">
			<h4>Dana Pensiun Telkom</h4>
			<p>Jln Surapati No.151 Bandung Jawa Barat Indonesia 40123</p>
		</div>
		<div class="col-sm-4">
			<h4>
				<strong>VISI</strong>
				<small>DANA PENSIUN TELKOM</small>
			</h4>
			<div>DANA PENSIUN TELKOM</div>
			<div>Manjadi Dana Pensiun Pemberi Kerja Terbaik di Indonesia</div>
		</div>
		<div class="col-sm-4">
			<h4>
				<strong>MISI</strong>
				<small>DANA PENSIUN TELKOM</small>
			</h4>
			<div>Memelihara kesinambungan pembayaran Manfaat Pensiun secara tepat waktu, jumlah dan penerima. Pensiun secara tepat waktu, jumlah dan penerima.</div>
			<div>Mengembangkan dana secara optimal dan aman serta mengkatkan pelayanan dengan mengoptimalkan SDM yang kopeten melalui praktek praktek terbaik.</div>
			<div>Memberikan hasil terbaik dan bermafaat bagi stakeholders</div>
		</div>
	</div>
</div>
