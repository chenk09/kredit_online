<?php
$this->breadcrumbs=array(
	'Mst Customers'=>array('index'),
	$model->id_customer,
);

$this->menu=array(
array('label'=>'List MstCustomers','url'=>array('index')),
array('label'=>'Create MstCustomers','url'=>array('create')),
array('label'=>'Update MstCustomers','url'=>array('update','id'=>$model->id_customer)),
array('label'=>'Delete MstCustomers','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_customer),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MstCustomers','url'=>array('admin')),
);
?>

<h1>View MstCustomers #<?php echo $model->id_customer; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_customer',
		'nama_customer',
		'city',
		'country',
		'penghasilan_perbulan',
		'email',
),
)); ?>
