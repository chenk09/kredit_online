<?php
$this->breadcrumbs=array(
	'Mst Customers',
);

$this->menu=array(
array('label'=>'Create MstCustomers','url'=>array('create')),
array('label'=>'Manage MstCustomers','url'=>array('admin')),
);
?>

<h1>Mst Customers</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
