<?php
$this->breadcrumbs=array(
	'Mst Customers'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MstCustomers','url'=>array('index')),
array('label'=>'Manage MstCustomers','url'=>array('admin')),
);
?>

<h1>Create MstCustomers</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>