<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_customer')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_customer),array('view','id'=>$data->id_customer)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_customer')); ?>:</b>
	<?php echo CHtml::encode($data->nama_customer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penghasilan_perbulan')); ?>:</b>
	<?php echo CHtml::encode($data->penghasilan_perbulan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>