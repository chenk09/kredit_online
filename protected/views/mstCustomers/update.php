<?php
$this->breadcrumbs=array(
	'Mst Customers'=>array('index'),
	$model->id_customer=>array('view','id'=>$model->id_customer),
	'Update',
);

	$this->menu=array(
	array('label'=>'List MstCustomers','url'=>array('index')),
	array('label'=>'Create MstCustomers','url'=>array('create')),
	array('label'=>'View MstCustomers','url'=>array('view','id'=>$model->id_customer)),
	array('label'=>'Manage MstCustomers','url'=>array('admin')),
	);
	?>

	<h1>Update MstCustomers <?php echo $model->id_customer; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>