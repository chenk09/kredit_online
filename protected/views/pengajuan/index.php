<?php $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => 'Daftar Customer',
        'headerIcon' => 'file',
        'headerButtons' => array(
            array(
                'class' => 'booster.widgets.TbButtonGroup',
				'size' => 'small',
				'buttonType' => 'link',
                'buttons' =>$this->menu,
            ),
        )
    )
);
?>
<?php echo $this->renderPartial('_form', array('mstCustomers'=>$model, 'trxPengajuan'=>$trxPengajuan)); ?>
<?php $this->endWidget(); ?>