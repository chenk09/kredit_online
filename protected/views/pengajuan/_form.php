<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'trx-pengajuan-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class'=>'col-sm-7'
	),
	'enableAjaxValidation'=>false,
)); ?>
<p class="help-block">Fields with <span class="required">*</span> are required.</p>
	<div class="form-group">
		<?php echo $form->labelEx($mstCustomers, "nama_customer", array(
			"class"=>"col-sm-3 control-label"
		)); ?>
		<div class="col-sm-9">
			<?php $this->widget(
				'booster.widgets.TbTypeahead',
				array(
					'model' => $mstCustomers,
					'attribute' => 'nama_customer',
					'datasets' => array(
						'source' => array(),
					),
					'htmlOptions'=>array(
						'class' => 'input-sm form-control',
					),
					'options' => array(
						'hint' => true,
						'highlight' => true,
						'minLength' => 1
					),
				)
			); ?>		
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($mstCustomers, "city", array(
			"class"=>"col-sm-3 control-label",
			"placeholder"=>$mstCustomers->getAttributeLabel('city')
		)); ?>
		<div class="col-sm-4">
			<?php echo $form->textField($mstCustomers,'city',array(
				'class'=>'input-sm form-control',
				'maxlength'=>32,
				"placeholder"=>$mstCustomers->getAttributeLabel('city')
			)); ?>
		</div>
		<div class="col-sm-5">
			<?php echo $form->textField($mstCustomers,'country',array(
				'class'=>'input-sm form-control',
				'maxlength'=>32,
				"placeholder"=>$mstCustomers->getAttributeLabel('country')
			)); ?>
		</div>
	</div>
	<?php echo $form->textFieldGroup($trxPengajuan,'penghasilan',array('widgetOptions'=>array('htmlOptions'=>array(
		'class'=>'nominal input-sm','maxlength'=>30
	)))); ?>
	<?php echo $form->emailFieldGroup($mstCustomers,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'input-sm','maxlength'=>30)))); ?>
	
	<div class="form-group">
		<div class="col-sm-3"></div>
		<div class="col-sm-9">
			<?php $this->widget('booster.widgets.TbButton', array(
					'icon'=>'save',
					'size'=>'small',
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>'Create',
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
