<?php
$this->breadcrumbs=array(
	'Daftar Pengajuan'
);
?>
<?php $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => 'Proses Pengajuan',
        'headerIcon' => 'list-alt',
        'headerButtons' => array(
            array(
                'class' => 'booster.widgets.TbButtonGroup',
				'size' => 'small',
				'buttonType' => 'link',
                'buttons' =>$this->menu,
            ),
        )
    )
);
?>
	
	<div class="row">
		<div class="col-sm-6">
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'trx-pengajuan-form',
				'type' => 'horizontal',
				'htmlOptions' => array(),
				'enableAjaxValidation'=>false,
			)); ?>
			<p class="help-block">Fields with <span class="required">*</span> are required.</p>
				<?php echo $form->textFieldGroup($mstCustomers,'nama_customer',array('widgetOptions'=>array('htmlOptions'=>array(
					'class'=>'nominal input-sm',
					'readonly'=>true,
					'maxlength'=>30
				)))); ?>
				<div class="form-group">
					<?php echo $form->labelEx($mstCustomers, "city", array(
						"class"=>"col-sm-3 control-label",
						"placeholder"=>$mstCustomers->getAttributeLabel('city')
					)); ?>
					<div class="col-sm-4">
						<?php echo $form->textField($mstCustomers,'city',array(
							'class'=>'input-sm form-control',
							'maxlength'=>32,
							'readonly'=>true,
							"placeholder"=>$mstCustomers->getAttributeLabel('city')
						)); ?>
					</div>
					<div class="col-sm-5">
						<?php echo $form->textField($mstCustomers,'country',array(
							'class'=>'input-sm form-control',
							'maxlength'=>32,
							'readonly'=>true,
							"placeholder"=>$mstCustomers->getAttributeLabel('country')
						)); ?>
					</div>
				</div>
				<?php echo $form->textFieldGroup($trxPengajuan,'penghasilan',array('widgetOptions'=>array('htmlOptions'=>array(
					'class'=>'nominal input-sm',
					'maxlength'=>30,
					'readonly'=>true,
				)))); ?>
				<?php echo $form->textAreaGroup($lstKomentar,'komentar',array('widgetOptions'=>array('htmlOptions'=>array(
					'class'=>'nominal input-sm',
					'maxlength'=>30
				)))); ?>
				
				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-9">
						<?php $this->widget('booster.widgets.TbButton', array(
								'icon'=>'save',
								'size'=>'small',
								'buttonType'=>'submit',
								'context'=>'success',
								'label'=>'Approve',
						)); ?>
						<?php $this->widget('booster.widgets.TbButton', array(
								'icon'=>'remove',
								'size'=>'small',
								'buttonType'=>'submit',
								'context'=>'danger',
								'label'=>'Tolak',
						)); ?>
					</div>
				</div>
			<?php $this->endWidget(); ?>		
		</div>
		<div class="col-sm-6">
			<legend>Histori Komentar</legend>
			<?php $this->widget('booster.widgets.TbGridView',array(
				'id'=>'trx-pengajuan-grid',
				'type'=>'bordered striped',
				'dataProvider'=>$lstKomentar->search(),
				'columns'=>array(
					'komentar',
					'status_pengajuan',
					'user_komentar',
					'tanggal_komentar'
				),
			)); ?>
		</div>
	</div>
	
<?php $this->endWidget(); ?>