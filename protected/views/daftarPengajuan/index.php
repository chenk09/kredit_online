<?php
$this->breadcrumbs=array(
	'Daftar Pengajuan'
);
?>
<?php $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => 'Daftar Pengajuan',
        'headerIcon' => 'list',
        'headerButtons' => array(
            array(
                'class' => 'booster.widgets.TbButtonGroup',
				'size' => 'small',
				'buttonType' => 'link',
                'buttons' =>$this->menu,
            ),
        )
    )
);
?>
	<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'trx-pengajuan-grid',
		'type'=>'bordered striped',
		'dataProvider'=>$model->search(),
		'columns'=>array(
			'id_pengajuan',
			'id_customer',
			'IdCustomer.nama_customer',
			'status_pengajuan',
			array
			(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{proses}',
				'buttons'=>array
				(
					'proses' => array
					(
						'icon'=>'wrench',
						'type'=>'link',
						'label'=>'PROSES PENGAJUAN',
						'url'=>'Yii::app()->createUrl("daftarPengajuan/create", array("id"=>$data->id_pengajuan))',
						'options'=>array(
							"class"=>"btn btn-sm btn-success"
						)
					)
				),
			)
		),
	)); ?>
<?php $this->endWidget(); ?>