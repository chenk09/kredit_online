<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'name'=>'KREDIT-ONLINE',
	'defaultController' => 'pengajuan',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	// preloading 'log' component
	'preload'=>array('log', 'booster'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	'aliases' => array(
		'booster' => realpath(__DIR__ . '/../vendor/yiibooster'),
		'scripts' => realpath(__DIR__ . '/../../scripts'),
		'css' => realpath(__DIR__ . '/../../css'),
	),
	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'war',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1', '*'),
			'generatorPaths' => array('booster.gii'),
		),
		'kepegawaian'
	),
	// application components
	'components'=>array(
		'booster' => array(
			'class' => 'booster.components.Booster',
		),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),		
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		// database settings are configured in database.php
		'db' => array(
			'class'=>'CDbConnection',
            'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
            'charset' => 'utf8',
			'enableParamLogging' => true
        ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'mail_conf'=>array(
			'type' => 'smtp',
			'host'=>'server_smtp',
			'port'=>25,
			'username'=>'username_masing',
			'password'=>'password_masing',
			'smtp_auth'=>false,
			'smtp_secure'=>"ssl",
			'from_name'=>"alamat_masing",
			'subject'=>"KREDIT ONLINE",
		)
	),
);
