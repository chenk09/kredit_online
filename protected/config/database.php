<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	//uncomment the following lines to use a MySQL database
	'class'=>'DbConnection',
	'connectionString' => "oci:dbname=10.15.2.118:1521/DPT;charset=UTF8",
	'username' => 'IFSAPP',
	'password' => 'IFSAPP',
	'enableProfiling' => true,
	'enableParamLogging' => true,
	'emulatePrepare' => true
	/*
	'connectionString' => 'mysql:host=localhost;dbname=dapen_hr',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'root',
	'charset' => 'utf8',
	*/
);