<?php

class Email
{
    public function send($to, $message)
    {
		$mailer = Yii::createComponent('application.extensions.mailer.EMailer');
		$emailadmin = Yii::app()->params['adminEmail'];
		
		$mailer->Host = Yii::app()->params['mail_conf']['host'];
		$mailer->Port = Yii::app()->params['mail_conf']['port'];
		
		if(Yii::app()->params['mail_conf']['smtp_auth']){
			$mailer->Username = Yii::app()->params['mail_conf']['username'];
			$mailer->Password = Yii::app()->params['mail_conf']['password'];
			$mailer->SMTPAuth = Yii::app()->params['mail_conf']['smtp_auth'];
			$mailer->SMTPSecure = Yii::app()->params['mail_conf']['smtp_secure'];
			$mailer->IsSMTP(Yii::app()->params['mail_conf']['type']);
		}else{
			$mailer->IsSMTP();
		}
		
		$mailer->IsSMTP(Yii::app()->params['mail_conf']['type']);
		$mailer->IsHTML(true);
		$mailer->From = $emailadmin;
		$mailer->AddReplyTo($emailadmin);
		$mailer->AddAddress($to);
		$mailer->FromName = Yii::app()->params['mail_conf']['from_name'];
		$mailer->CharSet = 'UTF-8';
		$mailer->Subject = Yii::app()->params['mail_conf']['subject'];
		$mailer->Body = $message;
		//objidposisi_parent

        try{
			if($mailer->Send()){
				$l = new EmailLog();
				$l->dest = $to;
				$l->status = 'OK';
				$l->t = null;
				$l->save();
				return true;
			}
	    	return false;
        }catch (phpmailerException $e){
            $l = new EmailLog();
            $l->dest = $to;
            $l->status = 'ERROR';
            $l->t = null;
            $l->errormsg = $e->getMessage();
            $l->save();
        }
	}

}
