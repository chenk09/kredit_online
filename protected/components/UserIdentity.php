<?php
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
		$user = Yii::app()->getComponent('user');
		$pengguna = array(
			'petugas_satu'=>'petugas_satu',
			'petugas_dua'=>'petugas_dua',
			'manager'=>'manager'
		);
		
		if(isset($pengguna[$this->username]))
		{
			if($pengguna[$this->username] == $this->password)
			{
				$this->errorCode=self::ERROR_NONE;
				$this->setState('id_sys_group', $this->username);
			}else{
				$user->setFlash('error',"<strong>Login Gagal!</strong> Password tidak sesuai.");
				$this->errorCode = self::ERROR_USERNAME_INVALID;			
			}
		}else $user->setFlash('error',"<strong>Login Gagal!</strong> Username tidak terdaftar.");
		
		return !$this->errorCode;
	}
}