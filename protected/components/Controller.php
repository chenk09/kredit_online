<?php
class Controller extends CController
{
	public $layout='//layouts/content';
	public $menu=array();
	public $user;
	public $breadcrumbs=array();
	
	public function init()
	{
		$this->user = Yii::app()->getComponent('user');
		Yii::app()->getClientScript()->registerScriptFile("scripts/main.js", CClientScript::POS_END);
		parent::init();
	}
	
	protected function beforeAction($event)
    {
		$js_file = Yii::getPathOfAlias('scripts') . "/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id . ".js";
		if(file_exists($js_file))
		{
			$_js_file =  "scripts/" . Yii::app()->controller->id . "/" . Yii::app()->controller->action->id  . ".js";
			Yii::app()->getClientScript()->registerScriptFile($_js_file, CClientScript::POS_END);
		}		
        return true;
    }	
	
}